classdef local_system_learning <handle
% Copyright (c) by Armin Lederer (TUM) under BSD License 
% Last modified: Armin Lederer 2022-02
    properties
        LoGGP;
        cloud_connect;
        
        % parameters for networked learning
        kc = 20;
        lambda = 1;
        delta = 0.01;
        Lf = 2;
        Lmutilde;
        rho = 1e-4;
        dof = 2;
        
        DT = 500; %measured in simulation steps
        dt = 1e-2;
        tau = 1e-2;
        Ns = 100;
        zeta = 0.01;
        Lxref = 10;
        counter = 1;
        odd = 0;
        Tp = 10;
        Mbar = 3000;
        Td = 0.1;
        B;
        stopLearn = 0;
        stopCount = 0;
        
        iactive = zeros(4,1000);
        
        P;
    end
    
    methods
        function obj = local_system_learning(in_pts,in_N, param)
            
            obj.kc = param.kc;
            obj.lambda = param.lambda;
            obj.delta = param.delta;
            obj.Lf = param.Lf;
            obj.rho = param.rho;
            obj.dof = param.dof;
            
            obj.dt = param.dt;
            obj.tau = param.tau;
            obj.Ns = param.Ns;
            obj.zeta = param.zeta;
            obj.Lxref = param.Lxref;
            obj.Tp = param.Tp;
            obj.Mbar = param.Mbar;
            obj.Td = param.Td;
            obj.B = param.B;
            
            obj.LoGGP = cell(obj.dof,1);
            for i=1:obj.dof
                obj.LoGGP{i} = gPoE_LoGGP(obj.dof*2,in_pts,in_N, [-pi/2*ones(2,1),pi/2*ones(2,1)]);
                obj.LoGGP{i}.sigmaF = param.sf;
                obj.LoGGP{i}.lengthS = param.l;
                obj.LoGGP{i}.sigmaN = param.sn;
                
                obj.LoGGP{i}.tau = param.tau;
                obj.LoGGP{i}.delta = param.delta;
                obj.LoGGP{i}.Lf = param.Lf;
                
            end
            
            Ai = [0,1;-obj.kc*obj.lambda, -obj.kc];
            A = zeros(2*obj.dof,2*obj.dof);
            for i=1:obj.dof
                A(2*i-1:2*i,2*i-1:2*i) = Ai;
            end
            
            try
                obj.P = icare(A,[],eye(obj.dof*2));
            catch
                obj.P = care(A,zeros(size(A)),eye(obj.dof*2));
            end
            curmod = mod(obj.odd+1,2)*obj.dof+(1:obj.dof);
            obj.iactive = zeros(2*obj.dof,1000);
            obj.iactive(curmod,1) = 1;
            obj.iactive(curmod,end) = 1;
            obj.Lmutilde = 1.5*obj.Lf;
            obj.DT = ceil(min(obj.Tp,obj.Mbar/(2*obj.B)+2*obj.Td)/obj.tau);
            obj.cloud_connect = cloud(obj.dof, in_pts, in_N, param);
            
            
        end
        
        function [numdat, tup,stopped]=update(obj,x,y,xref)
            
            tup = zeros(obj.dof,1);
            
            curmod = mod(obj.odd+1,2)*obj.dof+(1:obj.dof);
            prevmod = obj.odd*obj.dof+(1:obj.dof);
            
            %%check if leraning should be stopped
            numdat = zeros(obj.dof,1);
            if(~obj.stopLearn)
                for i=1:obj.dof%updates can be parallelized in practice
                    tic;
                    [children,parent] = obj.LoGGP{i}.update(x,y(i));
                    tup(i) = toc; 
                    obj.updateActive(curmod(i), children, parent);
                    obj.updateActive(prevmod(i), children, parent);
                end
            end
            
            stopped = obj.stopLearn;
            
            if(obj.counter > obj.DT)%Code not optimized for runtime from here onwards
                %count samples of previous models in memory
                num_prev = obj.countSamples(prevmod);
                
                %check if learning should be stopped
                if(any(num_prev>obj.Mbar/2-ceil(obj.Tp/obj.tau))||obj.stopCount>=1)
                    obj.stopCount = obj.stopCount+1;
                    if(obj.stopCount*obj.DT*obj.dt>=obj.Tp)
                        obj.stopLearn = 1;
                    end
                end
                
                %count samples of current models in memory
                num_cur = obj.countSamples(curmod);
                numdat = num_prev+num_cur;
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %transfer data
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                obj.transmit(prevmod)

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %delete unnecessary models
                obj.delete_unnecessary(prevmod, curmod);
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %receive data from the cloud
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                obj.receive(xref,prevmod);
                                
                obj.counter = 1;%reset counter
                obj.odd = mod(obj.odd+1,2);
            end
            obj.counter = obj.counter+1;
        end
        
        function out = predict(obj,x)
            out = zeros(obj.dof,1);
            for i=1:obj.dof
                out(i) = obj.LoGGP{i}.predict(x);
            end
        end
        
        function DT = getDT(obj)
            DT = obj.DT;
        end
        
        function updateActive(obj, idx, children, parent)
            if(~isempty(children)&&any(obj.iactive(idx,:)~=0))
                obj.iactive(idx,obj.iactive(idx,1:end-1)==parent) = children(1);
                obj.iactive(idx,obj.iactive(idx,end)+1:obj.iactive(idx,end)+length(children)-1) = children(2:end);
                obj.iactive(idx,end) = obj.iactive(idx,end)+length(children)-1;
            end
        end
        
        function num = countSamples(obj,idxs)
            num = zeros(obj.dof,1);
            for j = 1:obj.dof
                for i=1:length(obj.iactive(idxs(j),1:end-1))
                    if(obj.iactive(idxs(j),i)~=0)
                        num(j) = num(j) + obj.LoGGP{j}.localCount(obj.iactive(idxs(j),i));
                    end
                end
            end
        end
        
        
        function transmit(obj,idxs)
            if(~all(all(obj.iactive(idxs,:)==0)))
                for i=1:obj.dof
                    %prepare transmission structure
                    GP = gPoE_LoGGP(obj.dof*2,obj.LoGGP{i}.pts,obj.LoGGP{i}.N, [-pi/2*ones(2,1),pi/2*ones(2,1)]);
                    GP.medians = obj.LoGGP{i}.medians;
                    GP.parent = obj.LoGGP{i}.parent;
                    GP.children = obj.LoGGP{i}.children;
                    GP.overlaps = obj.LoGGP{i}.overlaps;
                    GP.auxUbic = obj.LoGGP{i}.auxUbic;
                    GP.Lmu = obj.LoGGP{i}.Lmu;
                    
                    GP.count = obj.LoGGP{i}.count;
                    for j=1:length(obj.iactive(idxs(i),1:end-1))
                        idx = obj.iactive(idxs(i),j);
                        if(idx~=0)
                            GP.localCount(idx) = obj.LoGGP{i}.localCount(idx);
                            GP.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                                obj.LoGGP{i}.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                            GP.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                                obj.LoGGP{i}.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                            GP.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                                obj.LoGGP{i}.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                            GP.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                                obj.LoGGP{i}.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                            GP.alpha(:,obj.LoGGP{i}.auxUbic(idx)) = obj.LoGGP{i}.alpha(:,obj.LoGGP{i}.auxUbic(idx));
                            GP.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx)) = obj.LoGGP{i}.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx));
                        end
                    end
                    obj.cloud_connect.receive(GP,i,obj.iactive(idxs(i),1:end-1));%receive data at the cloud
                end
            else%transmit the compete LoG-GP in the first iteration
                for i=1:obj.dof
                    GP = obj.LoGGP{i};
                    isactive = find(GP.auxUbic~=0);
                    obj.cloud_connect.receive(GP,i,isactive);%receive data at the cloud
                end
            end
        end
        
        function receive(obj,xref,idxs)
            reqmod = obj.cloud_connect.transmit(xref,[]);
            for i=1:obj.dof
                GP = obj.cloud_connect.transmit([],i);
                for j=1:length(reqmod(i,1:end-1))
                    idx = reqmod(i,j);
                    if(idx~=0)
                        obj.LoGGP{i}.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            GP.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            GP.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            GP.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            GP.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.alpha(:,obj.LoGGP{i}.auxUbic(idx)) = GP.alpha(:,obj.LoGGP{i}.auxUbic(idx));
                        obj.LoGGP{i}.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx)) = GP.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx));
                    end
                end
            end
            obj.iactive(idxs,:)=reqmod;
            for j=1:obj.dof
                for i=1:length(obj.iactive(1,1:end-1))
                    if(obj.iactive(j,i)~=0)
                        obj.iactive(j,end)=i;
                    end
                end
            end    
        end
        
        function delete_unnecessary(obj,idxsold, idxsnow)
            if(all(obj.iactive(idxsold,1)==1))
                unnecmod = zeros(obj.dof,size(obj.iactive(idxsold,:),2));
            else
                for i=1:obj.dof
                    unnecmod(i,:) = obj.iactive(idxsold(i),:);
                    for j=1:length(unnecmod(i,1:end-1))
                        if(unnecmod(i,j)~=0)
                            if(any(obj.iactive(idxsnow(i),1:end-1)==unnecmod(i,j)))
                                unnecmod(i,j) = 0;
                            end
                        end
                    end
                end
            end
            for i=1:obj.dof
                for j=1:length(unnecmod(i,1:end-1))
                    idx = unnecmod(i,j);
                    if(idx~=0)
                        obj.LoGGP{i}.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            zeros(obj.LoGGP{i}.xSize,obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            zeros(1,obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            zeros(obj.LoGGP{i}.pts,obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                            zeros(obj.LoGGP{i}.pts,obj.LoGGP{i}.pts);
                        obj.LoGGP{i}.alpha(:,obj.LoGGP{i}.auxUbic(idx)) = zeros(obj.LoGGP{i}.pts,1);
                        obj.LoGGP{i}.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx)) = zeros(obj.LoGGP{i}.pts,1);
                    end
                end
            end
        end
        
    end
    
end