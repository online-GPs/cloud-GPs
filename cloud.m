classdef cloud <handle
% Copyright (c) by Armin Lederer (TUM) under BSD License 
% Last modified: Armin Lederer 2022-02
    properties
        LoGGP;
        
        % parameters for networked learning
        kc = 50;
        lambda = 1;
        delta = 0.01;
        Lf = 1;
        Lmutilde;
        rho = 1e-4;
        r = 10;
        dof = 2;
        
        DT = 500; %measured in simulation steps
        dt = 1e-2;
        tau = 1e-2;
        Ns = 100;
        zeta = 0.01;
        Leta;
        Lxref = 10;
        counter = 1;
        odd = 0;
        Tp = 10;
        Mbar = 3000;
        Td = 0.1;
        B;
        stopLearn = 0;
        stopCount = 0;
        
        iactive = zeros(4,1000);
        
        P;
    end
    
    methods
        function obj = cloud(dof, in_pts, in_N, param)
            
            obj.kc = param.kc;
            obj.lambda = param.lambda;
            obj.delta = param.delta;
            obj.Lf = param.Lf;
            obj.rho = param.rho;
            obj.dof = param.dof;
            
            obj.dt = param.dt;
            obj.tau = param.tau-2;
            obj.Ns = param.Ns;
            obj.zeta = param.zeta;
            obj.Lxref = param.Lxref;
            obj.Tp = param.Tp;
            obj.Mbar = param.Mbar;
            obj.Td = param.Td;
            obj.B = param.B;
            
            obj.dof = dof;
            obj.LoGGP = cell(obj.dof,1);
            for i=1:obj.dof
                obj.LoGGP{i} = gPoE_LoGGP(obj.dof*2,in_pts,in_N, [-pi/2*ones(2,1),pi/2*ones(2,1)]);
                obj.Leta(i,1) = obj.LoGGP{i}.sigmaF*norm(obj.LoGGP{i}.lengthS);%multiplicity of standard deviation as approximation for Lipschitz constant
            end
            
            Ai = [0,1;-obj.kc*obj.lambda, -obj.kc];
            A = zeros(2*obj.dof,2*obj.dof);
            for i=1:obj.dof
                A(2*i-1:2*i,2*i-1:2*i) = Ai;
            end
            
            try
                obj.P = icare(A,[],eye(obj.dof*2));
            catch
                obj.P = care(A,zeros(size(A)),eye(obj.dof*2));
            end
            
            obj.Lmutilde = 1.5*obj.Lf;
        end
        
        function receive(obj,GP,i,isactive)
            obj.LoGGP{i}.medians = GP.medians;
            obj.LoGGP{i}.parent = GP.parent;
            obj.LoGGP{i}.children = GP.children;
            obj.LoGGP{i}.overlaps = GP.overlaps;
            obj.LoGGP{i}.auxUbic = GP.auxUbic;
            obj.LoGGP{i}.Lmu = GP.Lmu;
            
            obj.LoGGP{i}.count = GP.count;
            for j=1:length(isactive)
                idx = isactive(j);
                if(idx~=0)
                    obj.LoGGP{i}.localCount(idx) = GP.localCount(idx);
                    obj.LoGGP{i}.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                        GP.X(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                    obj.LoGGP{i}.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                        GP.Y(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                    obj.LoGGP{i}.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                        GP.K(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                    obj.LoGGP{i}.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts) = ...
                        GP.L(:,(obj.LoGGP{i}.auxUbic(idx)-1)*obj.LoGGP{i}.pts+1:obj.LoGGP{i}.auxUbic(idx)*obj.LoGGP{i}.pts);
                    obj.LoGGP{i}.alpha(:,obj.LoGGP{i}.auxUbic(idx)) = GP.alpha(:,obj.LoGGP{i}.auxUbic(idx));
                    obj.LoGGP{i}.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx)) = GP.auxAlpha(:,obj.LoGGP{i}.auxUbic(idx));
                end
            end
        end
        
        function GP = transmit(obj,xref,idx)
            if(isempty(idx))
                obj.findActive(xref);
                GP = obj.iactive(1:obj.dof,:);
            else
                GP = obj.LoGGP{idx};
            end
        end
        
        function findActive(obj,xref)
            newactive = zeros(obj.dof,size(obj.iactive,2));
            newactive(:,end) = 1;
            for n = 1:size(xref,2)
                
                eta = zeros(obj.dof,1);
                for j = 1:obj.dof
                    eta(j) = obj.LoGGP{j}.ebound(xref(:,n));
                end
                
                np = zeros(obj.dof,1);
                for j=1:obj.dof
                    np(j) = norm(obj.P(:,2*j));
                end
                eb_den = 1 - 2*(obj.Lf+obj.Lmutilde)*sum(np);
                if(eb_den<=0)
                    error('Control gain not large enough. Stability is not guaranteed');
                end
                
                eb = 2*(np'*eta)/eb_den*sqrt(max(eigs(obj.P)))/sqrt(min(eigs(obj.P)));
                
                xi = 2*obj.zeta+obj.Lxref*obj.dt/2+eb;
                
                
                
                %%%determine models for next cycle using random
                %%%sampling
                for j=1:obj.Ns
                    xtest = 2*xi*(rand(2*obj.dof,1)-0.5)+xref(:,n);
                    while(norm(xtest-xref(:,n))>xi)
                        xtest = 2*xi*(rand(2*obj.dof,1)-0.5)+xref(:,n);
                    end
                    
                    for k=1:obj.dof
                        ind = obj.LoGGP{k}.getActive(xtest);
                        
                        for l=1:length(ind)
                            if(not(any(newactive(k,1:end-1)==ind(l))))
                                newactive(k,newactive(k,end)+1) = ind(l);
                                newactive(k,end) = newactive(k,end)+1;
                            end
                        end
                    end 
                end    
            end
            obj.iactive(1:obj.dof,:) = newactive;
        end
    end
    
end