# Networked Online Learning Based on Gaussian Processes

The provided Matlab code accompanies the following work:
@unpublished{lederer2022networked,
  author    = {Lederer, Armin and Zhang, Mingmin and Tesfazgi, Samuel and Hirche, Sandra},
  title     = {Networked Online Learning for Control of Safety-Critical Resource-Constrained Systems based on {G}aussian Processes},
  year      = {2022},
}

Please acknowledge the authors in any academic publication that have made
use of this code or parts of it by referencing to the paper.

The most recent version is available at:
https://gitlab.lrz.de/online-GPs/cloud-GPs

Please send your feedbacks or questions to:
armin.lederer_at_tum.de
