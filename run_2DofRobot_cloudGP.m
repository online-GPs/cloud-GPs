% Copyright (c) by Armin Lederer (TUM) under BSD License 
% Last modified: Armin Lederer 2022-02
clearvars; clear; close all; %clc;rng default; 

%check Matlab version
vers=version('-release');
if(str2double(vers(end-2:end-1))<17)
    error('MATLAB R2017a or a newer release is required!');
end


%% download necessary external code

if(~exist('code_public','dir'))
    url = 'https://papers.nips.cc/paper/2019/file/fe73f687e5bc5280214e0486b273a5f9-Supplemental.zip';
    websave('NeurIPS_code',url);
    unzip('NeurIPS_code.zip');    
    %delete unnecessary files
    delete nips_2019_supplement.pdf
    rmdir('data','s')
    delete NeurIPS_code.zip
end

addpath(genpath('code_public'));

%% Set Parameters
disp('Setting Parameters...')
% Robot kinematrics
pdyn.L1 = 1; pdyn.L2 = 1; pdyn.R1 = pdyn.L1/2; pdyn.R2 = pdyn.L2/2;
pdyn.M1 = 1; pdyn.M2 = 1; pdyn.Iz1 = 1; pdyn.Iz2 = 1;
[pdyn.Mfun,pdyn.Cfun,pdyn.f] = getdyn2Dof(pdyn); pFeLi.Mfun = pdyn.Mfun;
nDof = 2;   % State space dimension

dt = 1e-2;              % sampling time
Nsim = 5000;             % Simulation steps
sn = 0.1;  % Observation noise (std deviation)
kc = 40;                % control gain
lambda =1;              % Hurwitz coefficient

% Initial State /reference for simulation
x0 = [0 0 0 0]';
period = 10;
ref{1} = @(t) refGeneral(t,2+1,@(tau) 0.5*sin(2*pi/period*tau));  % circle
ref{2} = @(t) refGeneral(t,2+1,@(tau) 1.5*sin(4*pi/period*tau));  % circle

% Controller gains
pFeLi.lam = lambda*ones(nDof,1);
pFeLi.kc = kc*ones(nDof,1);



%% define LoG-GP parameters and initialize
in_pts = 100;
in_N = 1000;

param.kc = kc;
param.lambda = lambda;
param.delta = 0.01;
param.Lf = 5;
param.rho = 1e-4;
param.dof = nDof;

param.dt = dt;
param.tau = 1e-2;%sample time in seconds
param.Ns = 100;
param.zeta = 0.01;
param.Lxref = 10;
param.Tp = 10; %period time in seconds
param.Mbar = 2*3000;%memory constrain in samples
param.Td = 0.1;%time delay in seconds
param.B = 6000;%bandwidth in samples per second

param.sn = sn;
param.l = ones(2*nDof,1);
param.sf = 1;



gp = local_system_learning(in_pts,in_N,param);

DT = gp.getDT();


%% Simulate System with Feedback Linearization
disp('Simulating Controlled System...')

pFeLi.f = @(x)gp.predict(x);
dyn = @(t,x) dynRobot(t,x,@(t,x) ctrlFeLiRob(t,x,pFeLi,ref),pdyn);

Xsim = x0;

numData = zeros(nDof,Nsim);
tup = zeros(nDof,Nsim);
stopped = zeros(1,Nsim);

for i=1:Nsim
    xn = ode45(dyn,[(i-1)*dt,i*dt],Xsim(:,end));
    xn = xn.y(:,end);
    yn = pdyn.f(xn)+sn*randn(2,1);
    tfut = linspace(i*dt+DT*dt,(i-1)*dt+2*DT*dt,DT);
    reftraj = ref{1}(tfut);
    xr(1:2,:) = reftraj(1:2,:);
    reftraj = ref{2}(tfut);
    xr(3:4,:) = reftraj(1:2,:);
    [numData(:,i),tup(:,i),stopped(:,i)]=gp.update(xn,yn,xr);
    pFeLi.f = @(x)gp.predict(x);
    Xsim = [Xsim,xn];
end

save('test');

%% illustration of the results

if(all(mean(tup,2)>param.tau))
    fprintf('Average prediction time of %0.2d is larger than sampling time %0.2d.\n',max(mean(tup,2)),param.tau)
    fprintf('Increase sampling time or try reducing in_pts to ensure satisfaction of the computational constraints!\n')
else
    fprintf('Average update time of %0.2d for joint 1 and %0.2d for joint 2 are smaller than the sampling time %0.2d.\n',mean(tup,2),param.tau)
    fprintf('Therefore, computational constraints are satisfied.\n')
end


tint = linspace(0,Nsim*dt,Nsim+1);
reftraj1 = ref{1}(tint); 
reftraj2 = ref{2}(tint); 
xr = [reftraj1(1,:); reftraj2(1,:)];
rmse = sqrt( sum((Xsim([1,3],:)-xr).^2,1) );
stopLearn = min(find(stopped>0));
figure(1);
semilogy(tint,movmean(rmse,1000))
xlabel('time [s]')
ylabel('RMSE [rad]')
title('Performance Increase over Time')
text(stopLearn*dt,2*rmse(1,stopLearn-1),'\rightarrow','FontSize',54,'Rotation',-90,...
    'HorizontalAlignment','center');
text(stopLearn*dt,3.8*rmse(1,stopLearn-1),'stopped learning','FontSize',14,...
    'HorizontalAlignment','center');


num = [zeros(2,1), numData(:,numData(1,:)~=0)];
tint = linspace(0,(size(num,2)-1)*DT*dt,size(num,2));
figure(2);
hold on;
axis([0, Nsim*dt, 0, 1.2*param.Mbar])
plot(tint,num)
plot([0,Nsim*dt],[param.Mbar,param.Mbar]);
xlabel('time [s]')
ylabel('local memory occupation [samples]')
title('Memory Occupation over Time')
text(stopLearn*dt,numData(1,stopLearn-1)+500,'\rightarrow','FontSize',54,'Rotation',-90,...
    'HorizontalAlignment','center')
text(stopLearn*dt,numData(1,stopLearn-1)+1400,'stopped learning','FontSize',14,...
    'HorizontalAlignment','center');

https://gitlab.lrz.de/alederer/test
